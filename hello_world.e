class HELLO_WORLD
--
-- The "Hello World" example.
--
--

insert
	ARGUMENTS

creation {ANY}
	main

feature {ANY}
	main is
		require
			un_argument_seulement: argument_count = 1
			est_entier: argument(1).is_integer
		local
			i: INTEGER
		do	
			from
			    i := 0 --initialize
			until
			    i = argument(1).to_integer --when do we stop looping?
			loop
				io.put_string(message(i) + "Hello World.%N")
			    i := i + 1 --increment i
			end
		end

	message (i: INTEGER): STRING is
		local
			resultat: STRING
		do
			if i.is_even
			then
			    resultat := i.to_string + " = Pair, "
			else
			    resultat := i.to_string + " = Impair, "
			end
			Result := resultat
		end
end -- class HELLO_WORLD
