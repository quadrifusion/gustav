class FIBONACCI

insert
	ARGUMENTS
	
creation {ANY}
	make

feature {ANY}
	tab: ARRAY[INTEGER]
	make is
		require
			un_argument_seulement: argument_count = 1
			est_entier: argument(1).is_integer
		do
			create tab.make(0, argument(1).to_integer)
			if argument_count /= 1 or not argument(1).is_integer then
				io.put_string("Utilisation : ")
				io.put_string(argument(0))
				io.put_string(" <entier>%N")
				die_with_code(exit_failure_code)
			end
			-- afficher l'array
			io.put_array(tab)
			io.put_new_line
		end

	fibonacci (i: INTEGER; tabindex: INTEGER): INTEGER is
		require
			i >= 0
		do
			tab.put(i, tabindex)
			if i <= 1 then
				Result:= i
			else
				Result:= fibonacci(i - 1, tabindex + 1) + fibonacci(i - 2, tabindex + 1)
			end
		end

end

