# gustav

Eiffel de ses morts

## Exemples

`
if [some condition] then
    [statement]
elseif
    [statement]
else
    [statement]
end
`

The elseif and else can be omitted

### switch

`
inspect val
    when [val1] then [statement]
    when [val2, val3] then [statement] --Eiffel allows multiple values to trigger a single case
    else
        [default statement] --no matching 'when' value found
    end
`

### Loops

Eiffel has two looping constructs, the across loop and the from-until loop.
Across

The across loop is a recent addition to the language, and provides a compact construct for iterating over a collection. The semantics of across is the same as the foreach loops known from both java and C#

example:

`
across collection as cursor loop 
    print (cursor.item) 
end
`

### From-until

The from-until loop is the swiss army knife of Eiffel loops (and until recently the only Eiffel loop). It takes up a lot of vertical screen space, but works just like you would expect.

`
from
    i := 0 --initialize
until
    i = 10 --when do we stop looping?
loop
    print(i) --do something with i
    i := i + 1 --increment i
end
`


or, looping over a collection of objects

`
from
    collection.start --initialize
until
    collection.after --stop when our collection pointer points after the last element
loop
    print(collection.item)
    collection.forth --increment the collection pointer
end
`

note that Eiffel has no keywords that interrupt program flow, such as break or continue. These can be simulated using boolean expressions and conditionals.
If Eiffel does not include your favorite looping construct, you can bash the from-until loop until it behaves like a

### while loop

`
from
    stop := false
until 
    stop = true
loop
    --do stuff
end
`

### do-while loop

`
from
    stop := false
    --do stuff
until
    stop = true
loop
    --do stuff again
end
`

